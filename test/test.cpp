/* * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	Copyright (C) 2007 by Andreas Bank, andreas.mikael.bank@gmail.com
 *	last change: 07-05-2011
 *
 * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "test.h"
#include "commctrl.h"
#include <stdio.h>

HINSTANCE			hInst;
void				myRegisterClass(HINSTANCE hInstance, LPSTR className, WNDPROC classFunction, int hMenu);
BOOL				showMainWnd(HINSTANCE, int);
void				showAboutBox(void);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	aboutBoxProc(HWND, UINT, WPARAM, LPARAM);
void				connectFunction(HWND);
LPSTR				itoc(int);	// int to char* function
LPSTR				errorh(void);
HWND				hWndTxbAddress, hWndTxbPort, hWndTxbToSend, hWndTxbReceive,
					hWndBtnGroup, hWndBtnConnect, hWndBtnExit;
LPSTR				errmsg;
int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	MSG msg;
	HACCEL hAccelTable;
	WSAData wsaData;
	if (WSAStartup(MAKEWORD(1, 1), &wsaData) != 0) {
		MessageBox(NULL, "WSAStartup (Windows networking init) failed.", "ERROR", MB_OK);
		PostQuitMessage(0);
	}
	InitCommonControls();
	myRegisterClass(hInstance, "myWinClass", (WNDPROC)WndProc, IDC_TEST);
	myRegisterClass(hInstance, "myAboutBoxClass", (WNDPROC)aboutBoxProc, 0);
	if (!showMainWnd (hInstance, nCmdShow)) {
		MessageBox(NULL, strcat(itoc((int)GetLastError()), "\r\n\r\nError creating main window"), "Error", MB_ICONEXCLAMATION | MB_OK);
		return FALSE;
	}
	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_TEST);
	while (GetMessage(&msg, NULL, 0, 0)) {
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return (int) msg.wParam;
}
void myRegisterClass(HINSTANCE hInstance, LPSTR className, WNDPROC classFunction, int classHMenu) {
	WNDCLASSEX wcex;
	wcex.cbSize			= sizeof(WNDCLASSEX); 
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= classFunction;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_MYICON);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW);
	wcex.lpszMenuName	= (LPCTSTR)classHMenu;
	wcex.lpszClassName	= className;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);
	if (RegisterClassEx(&wcex) == 0) {
		MessageBox(NULL, strcat("Error creating window.\r\n\r\nErr nr.: ", itoc((int)GetLastError())), "Error", MB_ICONEXCLAMATION | MB_OK);
		PostQuitMessage(0);
	}
}
void showAboutBox(HWND parentHandle) {
	HWND hWnd, hWndLblAbout, hWndBtnOk;
	hWnd = CreateWindow("myAboutBoxClass", "About", WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION, CW_USEDEFAULT, 0, 400, 120, parentHandle, NULL, hInst, NULL);
	if (hWnd == NULL) {
		MessageBox(NULL, itoc((int)GetLastError()), "ej", MB_OK);
		return;
	}
	hWndLblAbout	= CreateWindow("STATIC", "Copyright (C) 2007 Andreas Bank, andreas_bank@yahoo.se", WS_VISIBLE | WS_CHILD, 10, 10, 300,40, hWnd, NULL, hInst, NULL);
	hWndBtnOk		= CreateWindow("BUTTON", "E&xit", WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_DEFPUSHBUTTON, 150, 55, 60, 30, hWnd, (HMENU) IDC_BUTTON1, hInst, NULL);
	ShowWindow(hWnd, SW_SHOWNORMAL);
	UpdateWindow(hWnd);
}
BOOL showMainWnd(HINSTANCE hInstance, int nCmdShow) {
	HWND hWnd, hWndLblAddress, hWndLblPort, hWndLblToSend, hWndLblReceive;
	hInst = hInstance;
	hWnd = CreateWindow("myWinClass", "My app", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, 400, 400, NULL, NULL, hInstance, NULL);
	if (!hWnd) {
		return FALSE;
	}
	hWndLblAddress	= CreateWindow("STATIC", "Address to connect to:", WS_VISIBLE | WS_CHILD | WS_TABSTOP, 20, 75, 300,20, hWnd, (HMENU)IDC_STATIC, hInstance, NULL);
	hWndTxbAddress	= CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "dns.com", WS_VISIBLE | WS_CHILD | WS_TABSTOP, 20, 90, 300,20, hWnd, (HMENU) ID_EDIT_ADDRESS, hInstance, NULL);
	hWndLblPort		= CreateWindow("STATIC", "Port to connect to:", WS_VISIBLE | WS_CHILD | WS_TABSTOP, 20, 115, 300,20, hWnd, (HMENU)IDC_STATIC, hInstance, NULL);
	hWndTxbPort		= CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "80", WS_VISIBLE | WS_CHILD | WS_TABSTOP | ES_NUMBER, 20, 130, 300,20, hWnd, (HMENU) ID_EDIT_PORT, hInstance, NULL);
	hWndLblToSend	= CreateWindow("STATIC", "Data to send:", WS_VISIBLE | WS_CHILD | WS_TABSTOP, 20, 155, 300,20, hWnd, (HMENU)IDC_STATIC, hInstance, NULL);
	hWndTxbToSend	= CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "GET / HTTP/1.1\r\nHost: dns.com\r\nConnection: Close\r\n\r\n", WS_VISIBLE | WS_CHILD | WS_TABSTOP, 20, 170, 300,20, hWnd, (HMENU)ID_EDIT_TOSEND, hInstance, NULL);
	hWndLblReceive	= CreateWindow("STATIC", "Info:", WS_VISIBLE | WS_CHILD | WS_TABSTOP, 20, 195, 300,20, hWnd, (HMENU)IDC_STATIC, hInstance, NULL);
	hWndTxbReceive	= CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "info...", WS_VISIBLE | WS_CHILD | WS_TABSTOP | WS_VSCROLL | ES_MULTILINE | ES_AUTOVSCROLL, 20, 210, 360, 170, hWnd, (HMENU) ID_EDIT_INFO, hInstance, NULL);
	hWndBtnGroup	= CreateWindow("BUTTON", "Actions", WS_VISIBLE | WS_CHILD | BS_GROUPBOX, 10, 10, 150, 60, hWnd, (HMENU) ID_GROUP1, hInstance, NULL);
	hWndBtnConnect	= CreateWindow("BUTTON", "&Connect", WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_DEFPUSHBUTTON, 20, 30, 60, 30, hWnd, (HMENU) ID_SHOW_BUTTON, hInstance, NULL);
	hWndBtnExit		= CreateWindow("BUTTON", "E&xit", WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_PUSHBUTTON, 90, 30, 60, 30, hWnd, (HMENU) ID_EXIT_BUTTON, hInstance, NULL);
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	return TRUE;
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	int wmId, wmEvent;
	switch (message) {
	case WM_COMMAND:
		wmId    = LOWORD(wParam); 
		wmEvent = HIWORD(wParam); 
		switch (wmId) {
		case ID_SHOW_BUTTON:
			connectFunction(hWnd);
			break;
		case ID_EXIT_BUTTON:
			DestroyWindow(hWnd);
			break;
		case IDM_ABOUT:
			showAboutBox(hWnd);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_DESTROY:
		WSACleanup();
		PostQuitMessage(0);
		break;
	case WM_CTLCOLORSTATIC:
	{
		HDC hdcStatic = (HDC)wParam;
		SetTextColor(hdcStatic, RGB(0, 0, 0));
		SetBkMode(hdcStatic, TRANSPARENT);
	}
	break;
	case WM_SIZING:
		{
			RECT clientRect, *currentRect = (RECT*)lParam;
			BOOL processed = FALSE;
			GetClientRect(hWnd, &clientRect);
			SetDlgItemText(hWnd, ID_EDIT_TOSEND, itoc((int)clientRect.right));
			if (currentRect->right-currentRect->left < 400) {
				currentRect->right = currentRect->left+400;
				processed = TRUE;
			}
			if (currentRect->bottom-currentRect->top < 400) {
				currentRect->bottom = currentRect->top+400;
				processed = TRUE;
			}
			if (processed) return TRUE;
		}
		break;
	case WM_SIZE:
		{
			RECT clientRect;
			GetClientRect(hWnd, &clientRect);
			SetWindowPos(GetDlgItem(hWnd, ID_EDIT_INFO), HWND_NOTOPMOST, 20, 210, clientRect.right-40, clientRect.bottom-230, SWP_NOZORDER);
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
LRESULT CALLBACK aboutBoxProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case WM_COMMAND:
			if (LOWORD(wParam) == IDC_BUTTON1) {
				DestroyWindow(hWnd);
			}
			break;
		case WM_CTLCOLORSTATIC:
		{
			HDC hdcStatic = (HDC)wParam;
			SetTextColor(hdcStatic, RGB(0, 0, 0));
			SetBkMode(hdcStatic, TRANSPARENT);
		}
		break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
void connectFunction(HWND hWnd) {
	struct sockaddr_in dest_addr, my_addr;
	struct hostent *hostInfo;
	LPSTR hWndTxbAddressTxt = (LPSTR)malloc(sizeof(char)*100);
	LPSTR hWndTxbPortTxt = (LPSTR)malloc(sizeof(char)*100);
	LPSTR tmpCharPtr = (LPSTR)malloc(sizeof(char)*5120);
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	memset(hWndTxbAddressTxt, '\0', sizeof(hWndTxbAddressTxt));
	memset(hWndTxbPortTxt, '\0', sizeof(hWndTxbPortTxt));
	memset(tmpCharPtr, '\0', sizeof(tmpCharPtr));
	SendMessage(hWndTxbAddress, WM_GETTEXT, 100, (LPARAM) hWndTxbAddressTxt);
	hostInfo = gethostbyname(hWndTxbAddressTxt);
	my_addr.sin_addr.s_addr = INADDR_ANY;
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(0);
	memset(&(my_addr.sin_zero), '\0', sizeof(my_addr.sin_zero));
	dest_addr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *)hostInfo->h_addr_list[0])));
	dest_addr.sin_family = AF_INET;
	SendMessage(hWndTxbPort, WM_GETTEXT, 100, (LPARAM) hWndTxbPortTxt);
	dest_addr.sin_port = htons(atoi(hWndTxbPortTxt));
	memset(&(dest_addr.sin_zero), '\0', sizeof(dest_addr.sin_zero));
	sprintf(tmpCharPtr, "Connecting to %s on port %s", hWndTxbAddressTxt, hWndTxbPortTxt);
	SendMessage(hWndTxbReceive, WM_SETTEXT, 0, (LPARAM) tmpCharPtr);
	if (connect(sock, (struct sockaddr *) &dest_addr, sizeof(struct sockaddr)) == SOCKET_ERROR) {
		sprintf(tmpCharPtr, "Failed to connect to %s on port %s.\r\n%s", hWndTxbAddressTxt, hWndTxbPortTxt, errorh());
		SendMessage(hWndTxbReceive, WM_SETTEXT, 0, (LPARAM) tmpCharPtr);
	}
	else {
		sprintf(tmpCharPtr, "Connected to %s on port %s", hWndTxbAddressTxt, hWndTxbPortTxt);
		SendMessage(hWndTxbReceive, WM_SETTEXT, 0, (LPARAM) tmpCharPtr);
	}
	//SendMessage(hWndTxbToSend, WM_GETTEXT, 100, (LPARAM) tmpCharPtr);
	strcpy(tmpCharPtr, "GET / HTTP/1.1\r\nHost: txs.homeip.net\r\nConnection: Close\r\n\r\n");
	/*	I usually get into writing some nifty error handlers
	 *	for the send() and recv() but I'm too lazy for that right now
	 */
	send(sock, tmpCharPtr, (int)strlen(tmpCharPtr), 0);
	memset(tmpCharPtr, '\0', strlen(tmpCharPtr));
	recv(sock, tmpCharPtr, 5120, 0);
	SendMessage(hWndTxbReceive, WM_SETTEXT, 0, (LPARAM) strcat(tmpCharPtr, itoc((int)strlen(tmpCharPtr))));
	if (sock != NULL) {
		shutdown(sock, 0);
		closesocket(sock);
	}
}
LPSTR itoc(int number) {
	LPSTR returnChar = (LPSTR)calloc(sizeof(char), 10);
	sprintf(returnChar, "%d", number);
	return returnChar;
}
LPSTR errorh(void) {
	/* * * * * * * * * * * * * * * * * * * * * *
	 *	I only proccess the usual
	 *	socket errors for this application
	 * * * * * * * * * * * * * * * * * * * * * */
	int errnr = WSAGetLastError();
	switch(errnr) {
		case 10040:
			errmsg = (LPSTR) calloc(sizeof(char), strlen("Message too long"));
			strcpy(errmsg, "Message too long");
			break;
		case 10048:
			errmsg = (LPSTR) calloc(sizeof(char), strlen("Address already in use"));
			strcpy(errmsg, "Address already in use");
			break;
		case 10049:
			errmsg = (LPSTR) calloc(sizeof(char), strlen("Cannot assign requested address"));
			strcpy(errmsg, "Cannot assign requested address");
			break;
		case 10054:
			errmsg = (LPSTR) calloc(sizeof(char), strlen("Connection reset by peer"));
			strcpy(errmsg, "Connection reset by peer");
			break;
		case 10061:
			errmsg = (LPSTR) calloc(sizeof(char), strlen("Connection refused"));
			strcpy(errmsg, "Connection refused");
			break;
		case 10060:
			errmsg = (LPSTR) calloc(sizeof(char), strlen("Connection timed out"));
			strcpy(errmsg, "Connection timed out");
			break;
		default:
			errmsg = (LPSTR) calloc(sizeof(char), sizeof(errnr));
			_itoa(errnr, errmsg, 2);
			return errmsg;
	}
	return errmsg;
}