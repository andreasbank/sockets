//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by test.rc
//
#define IDI_MYICON                      2
#define IDM_ABOUT                       101
#define IDM_EXIT                        102
#define ID_EDIT_INFO                    103
#define IDI_SMALL                       104
#define IDC_TEST                        105
#define IDC_BUTTON1                     1000
#define ID_GROUP1                       1001
#define ID_SHOW_BUTTON                  1002
#define ID_EXIT_BUTTON                  1003
#define ID_EDIT_ADDRESS                 1004
#define ID_EDIT_PORT                    1005
#define ID_EDIT_TOSEND                  1006
#define ID_EDIT_RECEIVE                 1007
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         108
#define _APS_NEXT_SYMED_VALUE           106
#endif
#endif
